package task3;

import java.util.ArrayList;
import java.util.List;
import java.util.function.UnaryOperator;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> array = new ArrayList<String>(List.of("python", "java", "javascript", "pascal", "fortran"));
		
		UnaryOperator<List<String>> unary = (list) -> {
			List<String> newList = new ArrayList<String>();
			for (String string : list) {
				if(string.length()>5) {
					newList.add(string);
				}
			}
			return newList;
		};
		
		System.out.println(unary.apply(array));
	}

}
