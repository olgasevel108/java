package task1;

import java.util.ArrayList;
import java.util.List;
import java.util.function.UnaryOperator;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> array = new ArrayList<Integer>(List.of(1, 2, 3, 4, 5,6, 7, 8, 9));
		UnaryOperator<Integer> nullIn = (a) -> a%2 != 0 ? 0: a;
		
		array.replaceAll(nullIn);
		
		System.out.println(array);
		
	}

}
