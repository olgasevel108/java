package task2;

import java.util.function.UnaryOperator;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String name = "Hello 123 world";
		UnaryOperator<String> unary = (a) ->a.replaceAll("[^0-9]", "");
		
		System.out.println(unary.apply(name));
	
	}

}
