package task2;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] dependencies = {
	            "<dependency>",
	            "  <groupId>junit</groupId>",
	            "  <artifactId>junit</artifactId>",
	            "  <version>4.4</version>",
	            "  <scope>test</scope>",
	            "</dependency>",
	            "<dependency>",
	            "  <groupId>org.powermock</groupId>",
	            "  <artifactId>powermock-reflect</artifactId>",
	            "  <version>3.2</version>",
	            "</dependency>"
	        };
		
		List<String> result = Arrays.stream(dependencies)
				.filter(n -> n.startsWith("  <groupId>") && n.endsWith("</groupId>")) 
				.map( n -> n.replace("  <groupId>", "").replace("</groupId>", ""))
				.collect(Collectors.toList());

		System.out.println(result);
	}

}
