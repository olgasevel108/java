package task1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String text = "The quick brown fox jumped over the lazy dog";
		String[] temp = text.split(" ");
		List<String> array = new ArrayList<String>(Arrays.asList(temp));
		List<String> result =  array.stream()
		.filter(n -> n.length()<5 )
		.collect(Collectors.toList());
		for (String string : result) {
			System.out.print(string + " ");
		}
		
	}

}
