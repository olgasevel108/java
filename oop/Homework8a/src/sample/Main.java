package sample;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Container container = new Container();
		
		int [] numbers = new int[]{1, 2};
		String text = "i like Java";
		container.addClass(numbers.getClass());
		container.addClass(text.getClass());
		container.addClass(Double.class);
		
		Class topClass = container.getTopClass();
		System.out.println(topClass);
		
		container.removeClass(topClass);
		
		topClass = container.getTopClass();
		System.out.println(topClass);

		
	}

}
