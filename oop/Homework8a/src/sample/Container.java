package sample;

import java.util.Arrays;

public class Container {
	private Object [] arr;

	public Container() {
		arr = new Object[5];
	}
	
	public void addClass (Class clas) {
		int emptySlot = findEmptySlot();
		if (emptySlot != -1) {
			arr[emptySlot] = clas;
		}else {
			System.out.println("container is full");
		}
	}
	public void removeClass (Class clas) {
		int index = findClassIndex(clas);
		if (index != -1) {
			arr[index] = null;
		}else {
			System.out.println("class isn't found");
		}
	}
	
	public Class getTopClass() {
        for (int i = arr.length - 1; i >= 0; i--) {
            if (arr[i] != null) {
                return (Class) arr[i];
            }
        }
        return null; 
    }
	
	private int findEmptySlot() {
		for (int i = 0; i < arr.length; i++) {
			if(arr[i]==null) {
				return i;
			}
		}
		return -1;
	}
	
	
	private int findClassIndex(Class clas) {
		for (int i = 0; i < arr.length; i++) {
			if(arr[i]!=null && arr[i].equals(clas)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public String toString() {
		return "Container " + Arrays.toString(arr);
	}
	
	
	
}
