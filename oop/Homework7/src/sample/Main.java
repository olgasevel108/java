package sample;

public class Main {
	 /**
	 * @param args
	 */
	public static void main(String[] args) {
	        Port port = new Port();
	        Ship ship = new Ship("first ship", port);
	        Ship ship1 = new Ship("second ship", port);
	        Ship ship2 = new Ship("third ship ", port);

	        Thread th = new Thread(ship);
	        Thread th1 = new Thread(ship1);
	        Thread th2 = new Thread(ship2);

	        th.start();
	        th1.start();
	        th2.start();
	    }

}
