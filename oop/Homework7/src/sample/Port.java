package sample;

public class Port {
    private volatile int shipCounter = 0;

    public void unloadShip(Ship ship) {
        synchronized (this) {
            shipCounter += 1;

            while (shipCounter > 2) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        for (int i = 1; i <= 10; i += 1) {
            synchronized (this) {
                System.out.println(ship.getShipName() + " is unloading: " + i);
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        synchronized (this) {
            shipCounter -= 1;
            notifyAll();
        }
    }
}
