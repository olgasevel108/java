package sample;

public class Ship implements Runnable {
    private String shipName;
    private Port port;

    public Ship(String shipName, Port port) {
        super();
        this.shipName = shipName;
        this.port = port;
    }

    public Ship() {
        super();
        // TODO Auto-generated constructor stub
    }

    public String getShipName() {
        return shipName;
    }

    public void setShipName(String shipName) {
        this.shipName = shipName;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        port.unloadShip(this);
    }
}
