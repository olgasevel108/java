package sample2;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.function.BiConsumer;

public class Main {

    public static void main(String[] args) {

        Scanner sca = new Scanner(System.in);

        System.out.println("Введите текст: ");

        String text = sca.nextLine();

        
        String currentDirectory = System.getProperty("user.dir");
        String filename = currentDirectory + File.separator + "text.txt";
        System.out.println(filename);

        BiConsumer<String, String> fileWrite = (String textToWrite, String filenameToWrite) -> {
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(filenameToWrite, true))) {
            	bw.newLine();
            	bw.write(textToWrite);
                bw.newLine();
                System.out.println("Успешно записано в файл.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        };

        fileWrite.accept(text, filename);

        // Закрыть сканер после использования
        sca.close();
    }
}
