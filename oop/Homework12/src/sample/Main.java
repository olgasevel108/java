package sample;

import java.util.List;
import java.util.function.Consumer;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> array = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9);
		Consumer<Integer> printOdd = (Integer number) -> {
			if (number % 2 !=0) {
				System.out.println(number);
			}
		};
		
		array.forEach(printOdd);
	}

}
