package sample1;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> stringsList = List.of("java8", "react", "javascriptES12", "python", "python3");
		
		System.out.println("original list is:");
		for (String string : stringsList) {
			System.out.println(string);
		}
		
		List<String> reserve = new ArrayList<String>();
		Consumer<String> isNumber = (String str) ->{
			if(containsDigit(str)) {
				reserve.add(str);
			}
		};
		
		stringsList.forEach(isNumber);
		System.out.println();
		System.out.println("reserve list is:");
		for (String string : reserve) {
			System.out.println(string);
		}
	}
	
	public static boolean containsDigit(String str) {
		for(char c: str.toCharArray()) {
			if(Character.isDigit(c)) {
				return true;
			}
		}
		
		return false;
	}

}
