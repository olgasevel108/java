package sample1;

import java.util.Objects;

public class Cat implements Comparable<Cat> {
	private String name;
	private int age;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Cat(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}
	@Override
	public String toString() {
		return "Cat " + name + " " + age;
	}
	@Override
	public int compareTo(Cat o) {
		String name1 = this.getName();
		String name2 = o.getName();
		if(o==null) {
			throw new NullPointerException();
		}
		if (name1.length()>name2.length()) {
			return 1;
		}
		if (name1.length()<name2.length()) {
			return -1;
		} else {
			return 0;
		}
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if(this == null) {
			return false;
		}
		Cat other = (Cat) obj;
		
		return name == other.name && Objects.equals(age, other.age);
	}
	
	
	
	
	
}
