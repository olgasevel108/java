package sample;

public class Rectangle implements Comparable<Rectangle> {
	private int height;
	private int weight;
	
	
	
	public Rectangle(int height, int weight) {
		super();
		this.height = height;
		this.weight = weight;
	}
	public int getArea() {
		return height*weight;
	}
	@Override
	public int compareTo(Rectangle o) {
		double areaFirst = this.getArea();
		double areaSecond = o.getArea();
		if (o == null) {
			throw new NullPointerException();
		}
		if (areaFirst>areaSecond) {
			return 1;
		}
		if (areaFirst<areaSecond) {
			return -1;
		} else {
			return 0;
		}
		
		
	}
	@Override
	public String toString() {
		return "Rectangle " + height + " "+ weight;
	}
	
	
	
}
