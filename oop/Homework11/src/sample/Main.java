package sample;

import java.util.Arrays;
import java.util.Comparator;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Rectangle r1 = new Rectangle(1, 4);
		Rectangle r2 = new Rectangle(1, 5);
		Rectangle r3 = new Rectangle(1, 2);
		Rectangle r4 = new Rectangle(1, 1);
		Rectangle r5 = new Rectangle(1, 3);
		
		Rectangle [] rectangles = {r1, r2, r3, r4, r5};
		
		for (Rectangle rectangle : rectangles) {
			System.out.println(rectangle);
		}
		
		Comparator<Rectangle> rectangleComparator = new Comparator<Rectangle>() {
			@Override
			public int compare(Rectangle rect1, Rectangle rect2) {
				return rect1.compareTo(rect2);
			}
		};
		
		Arrays.sort(rectangles, rectangleComparator);
		
		
		System.out.println("sorted");
		for (Rectangle rectangle : rectangles) {
			System.out.println(rectangle);
		}
	}

}
