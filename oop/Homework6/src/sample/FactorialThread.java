package sample;

import java.math.BigInteger;

public class FactorialThread implements Runnable{
	private int number;

	public FactorialThread(int number) {
		super();
		this.number = number;
	}

	public int getNumber() {
		return number;
	}
	public BigInteger calculateFactorial(int number) {
		BigInteger fact = new BigInteger("1");
		fact = fact.multiply(new BigInteger(""+number));
		
		return fact;
	}

	@Override
	public void run() {
		Thread th = Thread.currentThread();
			System.out.println(th.getName()+" "+number+"!="+calculateFactorial(number));
		
	}
	
}
