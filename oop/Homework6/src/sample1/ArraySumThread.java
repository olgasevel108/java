package sample1;

import java.math.BigInteger;

public class ArraySumThread implements Runnable {
	private int[] array;
	private int firstIndex;
	private int lastIndex;
	private BigInteger sum = BigInteger.ZERO;
	
	
	public BigInteger getSum() {
		return sum;
	}

	
	
	public ArraySumThread(int[] array, int firstIndex, int lastIndex) {
		super();
		this.array = array;
		this.firstIndex = firstIndex;
		this.lastIndex = lastIndex;
	}



	@Override
	public void run() {
		// TODO Auto-generated method stub
		for (int i = firstIndex; i < lastIndex; i++) {
		    sum = sum.add(BigInteger.valueOf(array[i]));
		}
		

	}
	
	
}
