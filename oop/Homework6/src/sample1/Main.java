package sample1;

import java.math.BigInteger;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int [] array = new int [20000000];
		
		for (int i = 0; i < array.length; i++) {
			array[i] = i;
		}
		
		ArraySumThread th = new ArraySumThread(array, 0, 5000000);
		ArraySumThread th1 = new ArraySumThread(array, 5000001, 10000000);
		ArraySumThread th2 = new ArraySumThread(array, 10000001, 15000000);
		ArraySumThread th3 = new ArraySumThread(array, 15000001, 20000000);
		
		Thread thSum = new Thread(th);
		Thread thSum1 = new Thread(th1);
		Thread thSum2 = new Thread(th2);
		Thread thSum3 = new Thread(th3);
		
		long startTime = System.currentTimeMillis();
		

		thSum.start();
		thSum1.start();
		thSum2.start();
		thSum3.start();
		
		try {
			thSum.join();
			thSum1.join();
			thSum2.join();
			thSum3.join();
			
		} catch(InterruptedException e) {
			e.printStackTrace();
		}
		
		long endTime = System.currentTimeMillis();
		long time = endTime-startTime;
		System.out.println(th.getSum().add(th1.getSum().add(th2.getSum().add(th3.getSum()))));
		System.out.println("wated tyme by arraySumThread: "+time+"ms");
		
		
		
		
		// перевірка часу простого алгоритму
		
		long startTime1 = System.currentTimeMillis();
		BigInteger sum1 = BigInteger.ZERO;
		for (int i = 0; i < array.length; i++) {
			sum1 = sum1.add(BigInteger.valueOf(array[i]));
		}
		long endTime1 = System.currentTimeMillis();
		long time1 = endTime1-startTime1;
		System.out.println(sum1);
		System.out.println("wasted time by simple algoritm: "+time1+"ms");
	}

}
