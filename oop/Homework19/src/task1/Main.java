package task1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;


public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//List<Integer> numbers = new ArrayList<Integer>(List.of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
		Random random = new Random();
		List<Integer> numbers = random.ints(10, 0, 10).boxed().collect(Collectors.toList());
		
		String evenNumbers = numbers.stream().filter(a->a%2==0).map(Object::toString).collect(Collectors.joining(";"));
		
		String oddNumbers = numbers.stream().filter(a->a%2!=0).map(Object::toString).collect(Collectors.joining(";"));
		
		System.out.println(numbers);
		System.out.println("even numbers:" + evenNumbers);
		System.out.println("odd numbers:" + oddNumbers);
				
	}

}
