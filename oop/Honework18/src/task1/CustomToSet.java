package task1;

import java.util.*;
import java.util.function.*;
import java.util.stream.Collector;

public class CustomToSet{
	public static <T> Collector<T, Set<T>, Set<T>> toSet(){
		Supplier<Set <T>> supplier = HashSet::new;
		BiConsumer<Set<T>, T> accumulator = Set::add;
		BinaryOperator<Set<T>> combiner = (set1, set2) -> {
			set1.addAll(set2);
			return set1;
		};
		return Collector.of(supplier, accumulator, combiner, Collector.Characteristics.UNORDERED );
	}
}
