package sample;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Deque<String> array = new ArrayDeque<String>();
		array.add("Sheldon");
		array.add("Leonard");
		array.add("Volovits");
		array.add("Kutrapalli");
		array.add("Penny");
		System.out.println(array);
		for(int i=0; i<2 ; i+=1) {
			String value = array.pollFirst();
			array.addLast(value);
			array.addLast(value);
		}
		System.out.println(array);
	}

}
