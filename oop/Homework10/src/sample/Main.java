package sample;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Map<String, String> translator = new HashMap<String, String>(){{
            put("i", "Я");
            put("have", "маю");
            put("cat", "кішку");
            put("her", "її");
            put("name", "ім'я");
            put("lily", "Лілі");
            put("she", "вона");
            put("very", "дуже");
            put("friendly", "добра");
            put("and", "і");
            put("likes", "полюбляє");
            put("play", "грати");
        }};
        
        String filename = "English in.txt";
        
        List<String[]> text = new ArrayList<String[]>();
        try(BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            String line;
            while((line = reader.readLine()) != null) {
                System.out.println(line + "\n");
                String cleanLine = line.replace(".", "");
                String[] words = cleanLine.split(" ");
                text.add(words);
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        // запис перекладу
        String out = "Ukrainian out.txt";
        List<String> translateOut = new ArrayList<String>();
        
        for (String[] strings : text) {
            for (String word : strings) {
                word = word.replace(".", "");
                if(translator.containsKey(word.toLowerCase())) {
                    translateOut.add(translator.get(word.toLowerCase()));
                }
            }
        }

        System.out.println("text is ");
        
        for (String[] strings : text) {
            System.out.println(Arrays.toString(strings));
        }
        
        System.out.println("translate out is " + translateOut);
        
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(out))){
            for (String string : translateOut) {
                bw.write(string + " ");
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        addToDictionary(translator, "dog", "собака");
        addToDictionary(translator, "world", "світ");
        
        String filenameDictionary = "myDictionary.txt";
        
        saveTranslatorToDictionary(translator, filenameDictionary);
    }
    
    public static void addToDictionary(Map<String, String> dictionary, String englishWord, String ukrainianWord) {
    	dictionary.put(englishWord, ukrainianWord);
    }
    
    public static void saveTranslatorToDictionary(Map<String, String> dictionary, String filename) {
    	try(BufferedWriter bw = new BufferedWriter(new FileWriter(filename))){
    		for (Map.Entry<String, String> entry : dictionary.entrySet()) {
				bw.write(entry.getKey() + "=" + entry.getValue());
				bw.newLine();
			}
    	} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
