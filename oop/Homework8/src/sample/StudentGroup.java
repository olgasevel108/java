package sample;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class StudentGroup implements Serializable {
   private Student [] students;

   public StudentGroup(Student[] students) {
	   super();
	   this.students = students;
   }

public Student[] getStudents() {
	return students;
}

public void setStudents(Student[] students) {
	this.students = students;
}
   
   public void saveToFile(String fileName) {
	   try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(fileName))) {
           oos.writeObject(this);
       } catch (IOException e) {
           e.printStackTrace();
       }

   }
   
   public static StudentGroup loadFromFile(String fileName) {
	   try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("a.txt"))) {
           StudentGroup group = (StudentGroup) ois.readObject();
           System.out.println("data was loaded from " + fileName);
           return group;
       } catch (IOException e) {
           return null;
       } catch (ClassNotFoundException e) {
           return null;
       }

   }

}
