package sample;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Student s1 = new Student("vasya", "lebed", 25);
		Student s2 = new Student("grigoriy", "mahno", 20);
		Student s3 = new Student("mariya", "kishineva", 22);
		
		Student [] array = {s1, s2, s3};
		StudentGroup students = new StudentGroup(array);
		
		students.saveToFile("a.txt");
		
		StudentGroup loadedGroup = students.loadFromFile("a.txt");
		if (loadedGroup!=null) {
			System.out.println("data is found");
			for (Student student : array) {
				System.out.println(student.getName() + " " + student.getSurname() + " - " + student.getAge());
			}
		}
	}

}
