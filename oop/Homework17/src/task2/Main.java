package task2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> arrayNumber = new ArrayList<>(List.of(1, 2, 3, 12, 18, 11, 17, 15, 16, 19, 10, 13, 14));
		List<Integer> result = arrayNumber.stream()
				.filter(n -> n>10)
				.sorted(Comparator.comparingInt(Main::getLastChar))
				.collect(Collectors.toList());
		
		System.out.println(result);
	}
	public static int getLastChar(int number) {
		return number % 10;
	}
}
