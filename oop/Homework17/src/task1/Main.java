package task1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) {
		String text = "This is a sample text for sorting words by the number of vowels";
		List<String> array = new ArrayList(List.of(text.split(" ")));
		List<String> result = array.stream()
		.filter(n -> containsForCount(n))
		.sorted(Comparator.comparingInt(Main::count))
		.collect(Collectors.toList());
		System.out.println(result);
	}
	
	public static boolean containsForCount(String word) {
		for(char c: word.toCharArray()) {
			if("aeiouAEIOU".indexOf(c) != -1) {
				return true;
			}
			
		}
		return false;
	}
	public static int count(String word) {
		int result = 0;
		String vowels = "aeiouAEIOU";
		for(int ch: word.toCharArray()) {
			if(vowels.indexOf(ch)!=-1) {
				result += 1;
			}
		}
		return result;
	}

}
