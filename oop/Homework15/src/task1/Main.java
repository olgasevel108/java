package task1;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		Cat name1 = new Cat("vaska", 1);
		Cat name2 = new Cat("murzik", 2);
		Cat name3 = new Cat("barsik", 1);
		Cat name4 = new Cat("matynya", 1);
		
		List<Cat> cats = new ArrayList<Cat>(List.of(name1, name2, name3, name4));
		
		String result = cats.stream()
				.map(Cat::getName)
				.max((name1test, name2test) -> Integer.compare(name1test.length(), name2test.length()))
				.orElse("");
				
		
		System.out.println(result);
	}

}
