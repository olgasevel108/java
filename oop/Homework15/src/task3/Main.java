package task3;

import java.io.IOException;
import java.lang.foreign.Linker.Option;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.Optional;

public class Main {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			Optional<Path> maxFilePath = Files.walk(Paths.get(""))
					.filter(Files::isRegularFile)
					.max(Comparator.comparingLong(file -> {
						try {
							return Files.size(file);
						}catch(IOException e) {
							e.printStackTrace();
							return 0;
						}
					}));
			
			if(maxFilePath.isPresent()) {
				System.out.println(maxFilePath);
			}else {
				System.out.println("error");
			}
			} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
