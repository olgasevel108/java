package task2;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> list = new ArrayList<Integer>(List.of(1, 2, 3, 4, 5, 6, 7, 8, 9));
		
		List<Integer> result = list.stream()
				.filter(a -> a%2!=0)
				.sorted()
				.collect(Collectors.toList());
				
		System.out.println(result);
	}

}
