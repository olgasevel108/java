package task3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.BinaryOperator;

public class MinElementListBinaryOperator<T extends Comparable<T>> implements BinaryOperator<List<T>> {
    @Override
    public List<T> apply(List<T> list1, List<T> list2) {
        if (list1 == null || list1.isEmpty()) {
            return list2;
        } else if (list2 == null || list2.isEmpty()) {
            return list1;
        }

        T minElement1 = Collections.min(list1);
        T minElement2 = Collections.min(list2);

        if (minElement1.compareTo(minElement2) <= 0) {
            return list1;
        } else {
            return list2;
        }
    }
}
