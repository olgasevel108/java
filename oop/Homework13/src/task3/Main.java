package task3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.BinaryOperator;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BinaryOperator;

public class Main {
    public static void main(String[] args) {
        BinaryOperator<List<Integer>> minElementOperator = new MinElementListBinaryOperator<>();

        List<Integer> list1 = new ArrayList<>();
        list1.add(5);
        list1.add(0);
        list1.add(3);
        list1.add(4);

        List<Integer> list2 = new ArrayList<>();
        list2.add(10);
        list2.add(-2);
        list2.add(5);

        List<Integer> result = minElementOperator.apply(list1, list2);
        System.out.println("Список с минимальным элементом: " + result);
    }
}
