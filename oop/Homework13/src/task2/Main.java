package task2;

import java.util.List;
import java.util.ArrayList;
import java.util.function.BinaryOperator;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BinaryOperator<List<Integer>> binary = (a, b) ->{
			List<Integer> result = new ArrayList<Integer>();
			
			for (int string : a) {
				if(b.contains(string)) {
					if(!result.contains(string)) {
						result.add(string);
					}
				}
			}
			for (int string : b) {
				if(a.contains(string)) {
					if(!result.contains(string)) {
						result.add(string);
					}
					
				}
			}
			return result;
		};
		
		List<Integer> array = new ArrayList<Integer>(List.of(1, 2, 3, 4, 5, 6, 7, 8, 9));
		
		List<Integer> array1 = new ArrayList<Integer>(List.of(3, 4, 7));
		
		System.out.println(binary.apply(array, array1));
		
		
	}
	
}
