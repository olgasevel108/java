package task1;

import java.util.function.BinaryOperator;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BinaryOperator<String> word = (a, b) ->{
			String[] words1 = a.split(" ");
			String[] words2 = b.split(" ");
			String word1 = findLongestword(words1);
			String word2 = findLongestword(words2);
			
			if (word1.length()>word2.length()) {
				return word1;
			} else {
				return word2;
			}
		};
		
		String text = "The sun sets over the horizon";
		String text1 = "Dogs play joyfully in the park";
		
		System.out.println(word.apply(text, text1));
	}
	public static String findLongestword(String[]words) {
		String word = words[0];
		for (String string : words) {
			if (string.length()>word.length()) {
				word= string;
			}
			
		}
		return word;
	}
}
