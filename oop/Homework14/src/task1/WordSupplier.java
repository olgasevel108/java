package task1;

import java.util.function.Supplier;

public class WordSupplier implements Supplier<String> {
	private String[] words;
	private int currentIndex;
	public WordSupplier(String inputString) {
		this.words = inputString.split(" ");
		this.currentIndex = 0;
	}
	@Override
	public String get() {
		if(currentIndex<words.length) {
			String word = words[currentIndex];
			currentIndex++;
			return word;
		} else {
		return null;
	}
		}
	
	
	
}
