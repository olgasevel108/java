package task1;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String text = "I enjoy spending my weekends exploring new places and trying different types of food.";
		WordSupplier ws = new WordSupplier(text);
		while(true) {
			String word = ws.get();
			if(word!=null) {
				System.out.println(word);
			}else {
				break;
			}
		}
	
	}

}
