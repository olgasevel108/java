package sample;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        ToASCIart art = new ToASCIart();
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите слово:");
        String text = sc.nextLine().toUpperCase();
        char[] asciiText = text.toCharArray();
        
        //
        int asciiArtHeight = art.asciiArtMap.get(asciiText[0]).split("\n").length;

        
        
        for (int i=0; i < asciiArtHeight ; i+=1) {
			for (char symbol : asciiText) {
				String asciiArt = art.asciiArtMap.get(symbol);
				if(asciiArt != null) {
					String [] lines = asciiArt.split("\n");
					System.out.print(lines[i] + " ");
				}else {
					System.out.println(" ");
				}
				
			}
			System.out.println();
		}
    }
}
