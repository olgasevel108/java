package sample;

import java.util.Map;
import java.util.HashMap;

public class ToASCIart {
	HashMap<Character, String> asciiArtMap = new HashMap<>();
    
	public ToASCIart() {
		asciiArtMap.put('A', "  *  \n * * \n*****\n*   *\n*   *");
        asciiArtMap.put('B', "**** \n*   *\n**** \n*   *\n**** ");
        asciiArtMap.put('C', " *** \n*    \n*    \n*    \n *** ");
        asciiArtMap.put('D', "**** \n*   *\n*   *\n*   *\n**** ");
        asciiArtMap.put('E', "*****\n*    \n*****\n*    \n*****");
        asciiArtMap.put('F', "*****\n*    \n*****\n*    \n*    ");
        asciiArtMap.put('G', " *** \n*    \n*  **\n*   *\n *** ");
        asciiArtMap.put('H', "*   *\n*   *\n*****\n*   *\n*   *");
        asciiArtMap.put('I', "*****\n  *  \n  *  \n  *  \n*****");
        asciiArtMap.put('J', "*****\n   * \n   * \n*  * \n **  ");
        asciiArtMap.put('K', "*   *\n*  * \n**   \n*  * \n*   *");
        asciiArtMap.put('L', "*    \n*    \n*    \n*    \n*****");
        asciiArtMap.put('M', "*   *\n** **\n* * *\n*   *\n*   *");
        asciiArtMap.put('N', "*   *\n**  *\n* * *\n*  **\n*   *");
        asciiArtMap.put('O', " *** \n*   *\n*   *\n*   *\n *** ");
        asciiArtMap.put('P', "**** \n*   *\n**** \n*    \n*    ");
        asciiArtMap.put('Q', " *** \n*   *\n* * *\n*  **\n ** *");
        asciiArtMap.put('R', "**** \n*   *\n**** \n*  * \n*   *");
        asciiArtMap.put('S', " *** \n*    \n **  \n    *\n *** ");
        asciiArtMap.put('T', "*****\n  *  \n  *  \n  *  \n  *  ");
        asciiArtMap.put('U', "*   *\n*   *\n*   *\n*   *\n *** ");
        asciiArtMap.put('V', "*   *\n*   *\n * * \n * * \n  *  ");
        asciiArtMap.put('W', "*   *\n*   *\n* * *\n** **\n*   *");
        asciiArtMap.put('X', "*   *\n * * \n  *  \n * * \n*   *");
        asciiArtMap.put('Y', "*   *\n * * \n  *  \n  *  \n  *  ");
        asciiArtMap.put('Z', "*****\n   * \n  *  \n *   \n*****");
	}
    
}
