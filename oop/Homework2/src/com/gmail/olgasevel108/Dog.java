package com.gmail.olgasevel108;

public class Dog extends Animal {
	private String name;

	public Dog(String ration, String color, int weight, String name) {
		super(ration, color, weight);
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String getVoice() {
		return "oof oof";
	}
	
	@Override
	public void eat() {
		System.out.println(name + " eat " + getRation());
	}
	
	@Override
	public void sleep() {
		System.out.println(name + " is sleeping for 3 hours");
	}
	
}
