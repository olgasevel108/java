package com.gmail.olgasevel108;

public class Cat extends Animal {
	private String name;
	
	
	
	public Cat(String ration, String color, int weight, String name) {
		super(ration, color, weight);
		this.name = name;
	}

	@Override
	public String getVoice() {
		return "neow meow";
	}
	
	@Override
	public void eat() {
		System.out.println(name + " eats " + getRation());
	}
	
	@Override
	public void sleep() {
		System.out.println(name + "is sleeping now");
	}
}
