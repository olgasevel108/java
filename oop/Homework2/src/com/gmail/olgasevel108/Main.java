package com.gmail.olgasevel108;

public class Main {
    public static void main(String[] args) {
        Cat cat = new Cat("feed", "white", 5, "Murzik");
        Dog dog = new Dog("bones", "black", 10, "Smel'chak");
        
        System.out.println(dog.getVoice());
        dog.sleep();
        cat.eat();
        System.out.println(cat.getVoice());
        
        Veterinarian vet = new Veterinarian("Otto");
        vet.treatment(dog);
    }
}
