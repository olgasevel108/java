package com.gmail.olgasevel108;

import java.util.Scanner;

public class RecordStudent {
	private int id;
	private String groupName;
	private String name;
	private String lastName;
	private Gender gender;
	
	public Student recordStudent() {
		Scanner sca = new Scanner(System.in);
		
		System.out.println("print the id");
		id = sca.nextInt();
		sca.nextLine();
		
		System.out.println("print the groupname");
		groupName = sca.nextLine();
		
		System.out.println("Type gender");
        gender = Gender.valueOf(sca.nextLine());
        
		
		System.out.println("print the name");
		name = sca.nextLine();
		
		System.out.println("print the lastName");
		lastName = sca.nextLine();
		
		Student student = new Student(name, lastName, gender, id, groupName);
		return student;
		
	}
}
