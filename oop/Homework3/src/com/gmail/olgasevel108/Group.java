package com.gmail.olgasevel108;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Group {
	private String groupName;
	private List<Student> students;
	public Group() {
		super();
		students = new ArrayList<Student>();
	}
	public Group(String groupName, List<Student> students) {
		super();
		this.groupName = groupName;
		this.students = new ArrayList<>(students);
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public List<Student> getStudents() {
	    return new ArrayList<>(students);
	}

	
	@Override
	public String toString() {
		return "Group [groupName=" + groupName + ", students=" + students + "]";
	}
	
	
	public void addStudent(Student student) throws GroupOverflowException {
	    if (students.size() < 10) { 
	        students.add(student);
	    } else {
	        throw new GroupOverflowException();
	    }
	}
	
	public Student searchStudentByLastName(String lastName) throws StudentNotFoundException{
		for (int i = 0; i < students.size(); i++) {
			 if (students.get(i) != null) {
				if(students.get(i).getLastName().equals(lastName)) {
					return students.get(i);
				}
			}
		} throw new StudentNotFoundException();
	}

	
	public boolean removeStudentById(int id) {
	    for (int i = 0; i < students.size(); i++) {
	        Student student = students.get(i);
	        if (student != null && student.getId() == id) {
	            students.remove(i);
	            return true;
	        }
	    }
	    return false;
	}


	
	public List<Student> sortsHumanByLastName() {
		Collections.sort(students, new HumanComparator());
		return students;
	}
		
	}

