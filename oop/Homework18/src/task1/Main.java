package task1;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String string = "Life is like a camera. Just focus on what's important and capture the good times, develop from the negatives, and if things don't work out, just take another shot";
		
		List<String> text = new ArrayList<String>(List.of(string.split(" ")));
		
		BiFunction<Integer, String, Integer> biFunc = (a, b) -> a+b.length();
		
		BinaryOperator<Integer> binaryOp = (a, b) -> a+b;
		
		Integer totalSum = text.stream()
				.filter(a -> a.length()>4)
				.reduce(0, biFunc, binaryOp);
		
		System.out.println(totalSum);
	}

}
