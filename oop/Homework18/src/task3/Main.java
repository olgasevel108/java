package task3;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> numbers = new ArrayList<Integer>(List.of(1, 10, 3, 4, 5, 6, 7, 8, 9));
		
		BiFunction<Integer, Integer, Integer> bf = (a, b) -> a>b? a: b;
		
		BinaryOperator<Integer> bo = (a, b) -> a>b? a: b;
		
		Integer totalSum = numbers.stream().reduce(0, bf, bo);
		System.out.println(totalSum);
	}

}
