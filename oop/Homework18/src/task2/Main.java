package task2;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BinaryOperator;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> numbers = new ArrayList<Integer>(List.of(1, 2, 3, 4, 5, 6, 7, 8, 9));
		
		BinaryOperator<Integer> bo = (a, b) -> a*b;
		Optional<Integer> result = numbers.stream().reduce(bo);
		System.out.println(result);
	}

}
