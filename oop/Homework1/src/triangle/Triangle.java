package triangle;

public class Triangle {
	private int side1;
	private int side2;
	private int side3;
	
	public double getSquare(int side1, int side2, int side3) {
		int p = (side1+side2+side3)/2;
		double result = Math.sqrt((p*(p-side1)+p*(p-side2)+p*(p-side3)));
		return result;
	}
	
	public double getSqueareFromTriangle(Triangle element) {
		int p = (this.side1+this.side2+this.side3)/2;
		double result = Math.sqrt((p*(p-this.side1)+p*(p-this.side2)+p*(p-this.side3)));
		return result;
	}
	
	public Triangle(int side1, int side2, int side3) {
		
		this.side1 = side1;
		this.side2 = side2;
		this.side3 = side3;
	}



	public int getSide1() {
		return side1;
	}



	public void setSide1(int side1) {
		this.side1 = side1;
	}



	public int getSide2() {
		return side2;
	}



	public void setSide2(int side2) {
		this.side2 = side2;
	}



	public int getSide3() {
		return side3;
	}



	public void setSide3(int side3) {
		this.side3 = side3;
	}



	@Override
	public String toString() {
		return "Triangle [side1=" + side1 + ", side2=" + side2 + ", side3=" + side3 + "]";
	}
	
	
}
