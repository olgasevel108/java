package com.gmail.olgasevel108;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Shop product1 = new Shop(500, "our new shoes", 5);
		Shop product2 = new Shop(200, "best sales for the t-shirts!", 1);
		Shop product3 = new Shop(10000, "luxury dress", 1);
		
		Shop [] array = new Shop[] {product1, product2, product3};
		
		System.out.println(product1.makePurchase(array));
	}

}
