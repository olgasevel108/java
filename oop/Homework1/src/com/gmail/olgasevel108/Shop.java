package com.gmail.olgasevel108;

import java.util.Arrays;

public class Shop {
	private int price;
	private String desription;
	private int weight;
	
	
	
	
	public Shop(int price, String desription, int weight) {
		super();
		this.price = price;
		this.desription = desription;
		this.weight = weight;
	}

	public static String makePurchase(Shop...elements) {
		return "You have made a purchase: " +Arrays.toString(elements);
	}
	
	public int getPrice() {
		return price;
	}



	public void setPrice(int price) {
		this.price = price;
	}



	public String getDesription() {
		return desription;
	}



	public void setDesription(String desription) {
		this.desription = desription;
	}



	public int getWeight() {
		return weight;
	}



	public void setWeight(int weight) {
		this.weight = weight;
	}



	@Override
	public String toString() {
		return "Shop [price=" + price + ", desription=" + desription + ", weight=" + weight + "]";
	}
	
	
	
}
